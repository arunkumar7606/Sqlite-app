package com.example.b.sqliteapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION =1;
    public static final String DATABASE_NAME="ride_service";
    public static final String TABLE_NAME="registration_table";
    public static final String USER_ID="PERSON_ID";
    public static final String col1="First_name";
    public static final String col2="Last_name";
    public static final String col3="aadhar_card_number";
    public static final String col4="Driving_License";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query="CREATE TABLE " + TABLE_NAME + "(PERSON_ID INTEGER PRIMARY KEY AUTOINCREMENT ,First_name TEXT,Last_name TEXT,aadhar_card_number INTEGER, Driving_License TEXT);";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);

    }
}
